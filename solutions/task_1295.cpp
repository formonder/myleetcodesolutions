#include "pch.h"

/*
 *
 * Given an array nums of integers, return how many of them contain an even number of digits.
 *
 * Example 1:
 *
 * Input: nums = [12,345,2,6,7896]
 * Output: 2
 * Explanation:
 * 12 contains 2 digits (even number of digits).
 * 345 contains 3 digits (odd number of digits).
 * 2 contains 1 digit (odd number of digits).
 * 6 contains 1 digit (odd number of digits).
 * 7896 contains 4 digits (even number of digits).
 * Therefore only 12 and 7896 contain an even number of digits.
 *
 * Example 2:
 *
 * Input: nums = [555,901,482,1771]
 * Output: 1
 * Explanation:
 * Only 1771 contains an even number of digits.
 *
 *
 * Constraints:
 *
 * 1 <= nums.length <= 500
 * 1 <= nums[i] <= 10^5
 *
*/

int findNumbers(const std::vector<int>& nums)
{
    return static_cast<int>(count_if(cbegin(nums), cend(nums), [](int v) { return std::to_string(v).size() % 2 == 0;}));
}

TEST(FindNumbers, LeetCode_1295)
{
    ASSERT_EQ(findNumbers({                 }), 0);
    ASSERT_EQ(findNumbers({5, 901, 482, 1771}), 1);
    ASSERT_EQ(findNumbers({1, 2, 12, 14     }), 2);
}

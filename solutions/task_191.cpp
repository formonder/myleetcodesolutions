#include "pch.h"

/*
 * Write a function that takes an unsigned integer and return the number of '1'
 * bits it has (also known as the Hamming weight).
 *
 * Example 1:
 *
 * Input: 00000000000000000000000000001011
 * Output: 3
 * Explanation: The input binary string 00000000000000000000000000001011 has a
 * total of three '1' bits.
 *
 * Example 2:
 *
 * Input: 11111111111111111111111111111101
 * Output: 31
 * Explanation: The input binary string 11111111111111111111111111111101 has a
 * total of thirty one '1' bits.
 *
*/

int hammingWeight(uint32_t number)
{
    number = (number & 0x55555555) + ((number >> 1)  & 0x55555555);
    number = (number & 0x33333333) + ((number >> 2)  & 0x33333333);
    number = (number & 0x0F0F0F0F) + ((number >> 4)  & 0x0F0F0F0F);
    number = (number & 0x00FF00FF) + ((number >> 8)  & 0x00FF00FF);
    number = (number & 0x0000FFFF) + ((number >> 16) & 0x0000FFFF);

    return static_cast<int>(number);
}

TEST(Number_of_1_Bits, LeetCode_191)
{
    ASSERT_EQ(hammingWeight(0b00000000000000000000000000001011), 3);
    ASSERT_EQ(hammingWeight(0b11111111111111111111111111111101), 31);
    ASSERT_EQ(hammingWeight(0b00000000000000000000000010000000), 1);
}

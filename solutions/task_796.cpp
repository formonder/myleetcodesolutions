#include "pch.h"

/*
 * We are given two strings, A and B.
 *
 * A shift on A consists of taking string A and moving the leftmost character
 * to the rightmost position. For example, if A = 'abcde',
 * then it will be 'bcdea' after one shift on A.
 * Return True if and only if A can become B after some number of shifts on A.
 *
 * Example 1:
 *
 * Input: A = 'abcde', B = 'cdeab'
 * Output: true
 *
 * Example 2:
 *
 * Input: A = 'abcde', B = 'abced'
 * Output: false
*/

bool rotateString(std::string lhs, std::string rhs)
{
    if(lhs == rhs)
    {
        return true;
    }

    if(empty(lhs) || empty(rhs))
    {
        return false;
    }

    size_t numberOfPermutations = lhs.size();

    while(numberOfPermutations-- != 0)
    {
        rotate(begin(lhs), prev(end(lhs)), end(lhs));
        if(lhs == rhs)
        {
            return true;
        }
    }

    return false;
}

TEST(Rotate_String, LeetCode_796)
{
    ASSERT_TRUE(rotateString("abcde", "cdeab"));
    ASSERT_FALSE(rotateString("abcde", "abced"));
}

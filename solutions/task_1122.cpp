#include "pch.h"

/*
* Given two arrays arr1 and arr2, the elements of arr2 are distinct,
* and all elements in arr2 are also in arr1.
*
* Sort the elements of arr1 such that the relative ordering of items in arr1
* are the same as in arr2.  Elements that don't appear in arr2 should be
* placed at the end of arr1 in ascending order.
*
* Example 1:
*
* Input : arr1 = [2,3,1,3,2,4,6,7,9,2,19],
*         arr2 = [2,    1,4,3,  9,6     ]
* Output:        [2,2,2,1,4,3,3,9,6,7,19]
*
*/
using std::vector;

vector<int> relativeSortArray(const vector<int>& arr1, const vector<int>& arr2)
{
    vector<int> sortArray = arr1;
    auto it = begin(sortArray);

    for(const auto& elem : arr2)
    {
        it = partition(it, end(sortArray), [elem](int a) {return a == elem;});
    }

    sort(it, end(sortArray));

    return sortArray;
}

TEST(Relative_Sort_Array, LeetCode_1122)
{
    ASSERT_EQ(relativeSortArray({2,3,1,3,2,4,6,7,9,2,19}, {2,1,4,3,9,6}),
              vector<int>({2,2,2,1,4,3,3,9,6,7,19}));

    ASSERT_EQ(relativeSortArray({1,2,3,4,5}, {5,3}),
              vector<int>({5,3,1,2,4}));
}

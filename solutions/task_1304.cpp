#include "pch.h"

/*
 *
 * Given an integer n, return any array containing n unique integers such that they add up to 0.
 *
 * Example 1:
 *
 * Input: n = 5
 * Output: [-7,-1,1,3,4]
 * Explanation: These arrays also are accepted [-5,-1,1,2,3] , [-3,-1,2,-2,4].
 *
 * Example 2:
 *
 * Input: n = 3
 * Output: [-1,0,1]
 *
 * Example 3:
 *
 * Input: n = 1
 * Output: [0]
*/

using std::vector;

vector<int> sumZero(int n)
{
    vector<int> values(n, 0);

    auto middle = next(begin(values), values.size() / 2);

    iota(begin(values), middle, 1);

    const auto insertion_point = values.size() % 2 == 0 ? middle : next(middle);

    transform(begin(values), middle, insertion_point, [](int value) { return -(value); } );

    return values;
}

TEST(SumZero, leetcode_1304)
{
    auto ranges = sumZero(11);
    const int zero = 0;
    EXPECT_EQ(std::accumulate(begin(ranges), end(ranges), 0), zero);
}

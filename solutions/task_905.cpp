#include "pch.h"

/*
 * Given an array A of non-negative integers, return an array consisting of all
 * the even elements of A, followed by all the odd elements of A.
 *
 * You may return any answer array that satisfies this condition.
 *
 * Example 1:
 *
 * Input: [3,1,2,4]
 * Output: [2,4,3,1]
 * The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.
*/

using std::vector;

vector<int> sortArrayByParity(const vector<int>& v)
{
    vector<int> result = v;
    auto it = partition(begin(result), end(result), [] (int a) {return a % 2 == 0;});
    sort(begin(result), it);
    sort(it, end(result));

    return result;
}

TEST(Sort_Array_By_Parity, LeetCode_905)
{
    ASSERT_EQ(sortArrayByParity({1,2,3,4}), vector<int>({2,4,1,3}));
}

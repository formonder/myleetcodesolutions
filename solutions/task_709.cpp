#include "pch.h"

/*
 * Implement function ToLowerCase() that has a string parameter str,
 * and returns the same string in lowercase.
 *
 *
 * Example 1:
 *
 * Input: "Hello"
 * Output: "hello"
 *
 * Example 2:
 *
 * Input: "LOVELY"
 * Output: "lovely"
*/

std::string toLowerCase(std::string str)
{
    std::transform(begin(str), end(str), begin(str), [](unsigned char c) {return std::tolower(c);});
    return str;
}

TEST(To_Lower_Case, LeetCode_709)
{
    ASSERT_EQ(toLowerCase("GoodBye"), "goodbye");
    ASSERT_EQ(toLowerCase("HELLO"), "hello");
}

#include "pch.h"

/*
 * Given an array of integers arr, write a function that returns true if
 * and only if the number of occurrences of each value in the array is unique.
 *
 * Example 1:
 *
 * Input: arr = [1,2,2,1,1,3]
 * Output: true
 * Explanation: The value 1 has 3 occurrences, 2 has 2 and 3 has 1.
 * No two values have the same number of occurrences.
 *
 * Example 2:
 *
 * Input: arr = [1,2]
 * Output: false
 *
*/

using std::unordered_map;
using std::unordered_set;
using std::vector;

bool uniqueOccurrences(const vector<int>& arr)
{
    unordered_map<int, int> values_count;
    for(int val : arr)
    {
        values_count[val]++;
    }

    unordered_set<int> result;
    for(auto [number, count] : values_count)
    {
        if(!result.insert(count).second)
        {
            return false;
        }
    }

    return true;
}

TEST(Unique_Number_of_Occurrences, LeetCode_1207)
{
    ASSERT_TRUE(uniqueOccurrences({1,2,2,1,1,3}));
    ASSERT_TRUE(uniqueOccurrences({-3,0,1,-3,1,1,1,-3,10,0}));
    ASSERT_FALSE(uniqueOccurrences({1,2}));
}

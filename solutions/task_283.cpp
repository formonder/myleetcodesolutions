#include "pch.h"

/*
 * Given an array nums, write a function to move all 0's to the end of it while
 * maintaining the relative order of the non-zero elements.
 *
 * Example:
 *
 * Input: [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 *
 * Note:
 *
 * You must do this in-place without making a copy of the array.
 * Minimize the total number of operations.
 *
*/

using std::vector;

void moveZeroes(vector<int>& nums)
{
    stable_partition(begin(nums), end(nums), [](int a) {return a != 0;});
}

TEST(Move_Zeroes, LeetCode_283)
{
    vector<int> v {0,1,0,3,12};
    vector<int> expected {1,3,12,0,0};
    moveZeroes(v);
    ASSERT_EQ(v, expected);
}

cmake_minimum_required(VERSION 3.8)

set(This LeetCodeSolutionsTests)

file(GLOB Sources "*.cpp")
file(GLOB Headers "*.h")

target_include_directories(
    ${PROJECT_NAME} PUBLIC
    ${PROJECT_SOURCE_DIR}
)

add_executable(${This} ${Sources} ${Headers})

target_link_libraries(
    ${This} PUBLIC
    gtest_main)

add_test(
    NAME ${This}
    COMMAND ${This}
)

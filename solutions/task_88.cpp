#include "pch.h"

/*
 * Given two sorted integer arrays nums1 and nums2,
 * merge nums2 into nums1 as one sorted array.
 *
 * Note:
 * The number of elements initialized in nums1 and nums2 are m and n respectively.
 * You may assume that nums1 has enough space (size that is greater
 * or equal to m + n) to hold additional elements from nums2.
 *
 * Example:
 *
 * Input:
 * nums1 = [1,2,3,0,0,0], m = 3
 * nums2 = [2,5,6],       n = 3
 *
 * Output: [1,2,2,3,5,6]
 *
 *
*/

using std::vector;

void merge(vector<int>& nums1, int m, vector<int>& nums2, int n)
{
    vector<int> result;
    result.reserve(nums1.size() + nums2.size());
    merge(begin(nums1), begin(nums1) + m, begin(nums2), begin(nums2) + n, std::back_inserter(result));
    swap(nums1, result);
}

TEST(Merge, LeetCode_88)
{
    vector<int> arr1 {1,3,5,1,3,4};
    vector<int> arr2 {2,4,6};
    vector<int> expected {1,2,3,4,5,6};
    merge(arr1, 3, arr2, 3);

    ASSERT_TRUE(arr1 == expected);
}

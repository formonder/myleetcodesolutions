#include "pch.h"

/*
 * Task 912. Sort an Array
 *
 * Given an array of integers nums, sort the array in ascending order.
*/

using std::vector;

vector<int> sortArray(const vector<int>& nums)
{
    vector<int> sortedArray = nums;
    sort(begin(sortedArray), end(sortedArray));
    return sortedArray;
}

TEST(SortArray, LeetCode_912)
{
    ASSERT_EQ(sortArray({3, 2, 1}), vector<int>({1,2,3}));
    ASSERT_EQ(sortArray({1}), vector<int>({1}));
    ASSERT_EQ(sortArray({}), vector<int>({}));
}

#include "pch.h"

/*
 * You're given strings J representing the types of stones that are jewels,
 * and S representing the stones you have.
 * Each character in S is a type of stone you have.
 * You want to know how many of the stones you have are also jewels.
 *
 * The letters in J are guaranteed distinct,
 * and all characters in J and S are letters. Letters are case sensitive,
 *  so "a" is considered a different type of stone from "A".
 *
 * Example 1:
 *
 * Input: J = "aA", S = "aAAbbbb"
 * Output: 3
 *
 * Example 2:
 *
 * Input: J = "z", S = "ZZ"
 * Output: 0
*/

int numJewelsInStones(const std::string &jewels, const std::string &stones)
{
    int countJewels = 0;
    for_each(begin(jewels),
             end(jewels),
             [&](char c){ countJewels += count(begin(stones), end(stones), c); });

    return countJewels;
}

TEST(Jewels_and_Stones, LeetCode_771)
{
    ASSERT_EQ(numJewelsInStones("aA", "aAAbcde"), 3);
    ASSERT_EQ(numJewelsInStones("z", "ZZ"), 0);
    ASSERT_EQ(numJewelsInStones("", "aa"), 0);
    ASSERT_EQ(numJewelsInStones("a", ""), 0);
}

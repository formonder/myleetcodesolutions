#include "pch.h"

/*
 * Given a string, determine if it is a palindrome, considering only
 * alphanumeric characters and ignoring cases.
 *
 * Note:
 * For the purpose of this problem, we define empty string as valid palindrome.
 *
 * Example 1:
 *
 * Input: "A man, a plan, a canal: Panama"
 * Output: true
 *
 * Example 2:
 *
 * Input: "race a car"
 * Output: false
*/

bool isPalindrome(std::string s)
{
    if(empty(s) || all_of(begin(s), end(s), [&s](char c) {return c == s.front();}))
    {
        return true;
    }

    s.erase(remove_if(begin(s), end(s),
                      [](unsigned char c){return ispunct(c) || isspace(c);}), end(s));
    auto left = begin(s);
    auto right = prev(end(s));
    while(left < right)
    {
        if(tolower(*left++) != tolower(*right--)) return false;
    }

    return true;
}

TEST(Valid_Palindrome, LeetCode_125)
{
    ASSERT_TRUE(isPalindrome("A man, a plan, a canal: Panama"));
    ASSERT_FALSE(isPalindrome("race a car"));
}

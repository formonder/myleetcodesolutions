#include "pch.h"

/*
 * There are two sorted arrays nums1 and nums2 of size m and n respectively.
 *
 * Find the median of the two sorted arrays.
 * The overall run time complexity should be O(log (m+n)).
 *
 * You may assume nums1 and nums2 cannot be both empty.
 *
 * Example 1:
 *
 * nums1 = [1, 3]
 * nums2 = [2]
 * The median is 2.0
 *
 * Example 2:
 *
 * nums1 = [1, 2]
 * nums2 = [3, 4]
 * The median is (2 + 3)/2 = 2.5
*/
using std::vector;

double findMedianSortedArrays(vector<int> &nums1, vector<int> &nums2)
{
    nums1.insert(nums1.end(), nums2.begin(), nums2.end());
    size_t numSize = nums1.size();

    nth_element (nums1.begin(), nums1.begin() + numSize / 2, nums1.end());

    double middle = nums1[numSize / 2];

    if(numSize % 2 == 0)
    {
        nth_element (nums1.begin(), nums1.begin() + (numSize - 1) / 2, nums1.end());
        middle += nums1[(numSize- 1) / 2];
        middle /= 2;
    }

    return middle;
}

TEST(Median_of_Two_Sorted_Arrays, LeetCode_4)
{
    vector<int> arr1, arr2;
    arr1 = {1,1,3,3};
    arr2 = {1,1,3,3};
    ASSERT_DOUBLE_EQ(findMedianSortedArrays(arr1, arr2), 2.0);

    arr1 = {2};
    arr2 = {};
    ASSERT_DOUBLE_EQ(findMedianSortedArrays(arr1, arr2), 2.0);

    arr1 = {6,7,8,9,10};
    arr2 = {1,2,3,4,5};
    ASSERT_DOUBLE_EQ(findMedianSortedArrays(arr1, arr2), 5.5);

    arr1 = {1,2,3,4,5};
    arr2 = {1,2,2,3,4};
    ASSERT_DOUBLE_EQ(findMedianSortedArrays(arr1, arr2), 2.5);
}

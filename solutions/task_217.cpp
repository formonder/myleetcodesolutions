#include "pch.h"

/*
 *Given an array of integers, find if the array contains any duplicates.
 *
 * Your function should return true if any value appears at least twice in the array,
 * and it should return false if every element is distinct.
 *
 * Example 1:
 *
 * Input: [1,2,3,1]
 * Output: true
 *
 * Example 2:
 *
 * Input: [1,2,3,4]
 * Output: false
 *
*/

using std::vector;

bool containsDuplicate(const vector<int>& nums)
{
    vector<int> result = nums;
    sort(begin(result), end(result));
    return adjacent_find(begin(result), end(result)) == end(result) ? false : true;
}

TEST(Contains_Duplicate, LeetCode_217)
{
    ASSERT_EQ(containsDuplicate({1,2,3,4}), false);
    ASSERT_EQ(containsDuplicate({1,1,1,3,3,4,3,2,4,2}), true);
}

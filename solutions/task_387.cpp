#include "pch.h"

/*
 * Task 387. First Unique Character in a String.
 *
 * Given a string, find the first non-repeating character
 * in it and return it's index. If it doesn't exist, return -1.
 *
 * Examples:
 *
 * s = "leetcode"
 * return 0.
 *
 * s = "loveleetcode",
 * return 2.
 *
 * Note: You may assume the string contain only lowercase letters.
*/

int firstUniqChar(const std::string &s)
{

    auto posInAlphabet = [](unsigned char c) -> size_t { return c - 97; };
    std::array<int, 26> letters{};
    for_each(cbegin(s), cend(s), [&](unsigned char c) { letters[posInAlphabet(c)]++; });
    const auto it = std::find_if(cbegin(s), cend(s),
                                 [&](unsigned char c)
                                 { return letters[posInAlphabet(c)] == 1; });

    return it == cend(s) ? -1 : static_cast<int>(distance(cbegin(s), it));
}

TEST(FirstUniqChar, LeetCode_387)
{
    ASSERT_EQ(firstUniqChar("leetcode"), 0);
    ASSERT_EQ(firstUniqChar("abac"), 1);
    ASSERT_EQ(firstUniqChar(""), -1);
    ASSERT_EQ(firstUniqChar("abcabc"), -1);
}

#include "pch.h"

/*
 *
 * Given a string S, return the "reversed" string where all characters
 * that are not a letter stay in the same place,
 * and all letters reverse their positions.
 *
 * Example 1:
 *
 * Input: "ab-cd"
 * Output: "dc-ba"
 *
 * Example 2:
 *
 * Input: "a-bC-dEf-ghIj"
 * Output: "j-Ih-gfE-dCba"
 *
 * Example 3:
 *
 * Input: "Test1ng-Leet=code-Q!"
 * Output: "Qedo1ct-eeLg=ntse-T!"
 *
*/

std::string reverseOnlyLetters(std::string s)
{
    partition(begin(s), end(s), [b = true](auto e) mutable
                                {
                                    if(isalpha(e))
                                        b = !b;
                                    return b;
                                });
    return s;
}

TEST(ReverseOnlyLetters, LeetCode_917)
{
    EXPECT_EQ(reverseOnlyLetters("Test1ng-Leet=code-Q!"), "Qedo1ct-eeLg=ntse-T!");
}

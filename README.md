В данном репозитории собраны мои решения различных задач с сайта: https://leetcode.com/

Все решения задач написаны с применением языка программирования C++.

В задачах будут применяться STL "алгоритмы, контейнеры, итераторы, ..."

И мы увидим, как применяя C++ многие задачи решаются очень лаконично и элегантно.

Список ссылок для более глубокого ознакомления с STL алгоритмами и не только:

Jonathan Boccara: https://www.youtube.com/watch?v=2olsGf6JIkU

Sean Parent: https://www.youtube.com/watch?v=W2tWOdzgXHA

Conor Hoekstra 1: https://www.youtube.com/watch?v=pUEnO6SvAMo

Conor Hoekstra 2: https://www.youtube.com/watch?v=sEvYmb3eKsw

Marshall Clow: https://www.youtube.com/watch?v=h4Jl1fk3MkQ